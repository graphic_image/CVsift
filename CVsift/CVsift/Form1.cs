﻿using Emgu.CV;
using Emgu.CV.Structure;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Diagnostics;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace CVsift
{
    public partial class Form1 : Form
    {
        public Form1()
        {
            InitializeComponent();
        }

        string fName;
        string inifname = Application.StartupPath;
        private void button1_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog = new OpenFileDialog();
            openFileDialog.Filter = "bmp|*.bmp|jpg|*.jpg";
            openFileDialog.RestoreDirectory = true;
            openFileDialog.FilterIndex = 1;
            openFileDialog.InitialDirectory = inifname;
            if (openFileDialog.ShowDialog() == DialogResult.OK)
            {
                fName = openFileDialog.FileName;
                inifname = fName.Replace(openFileDialog.SafeFileName, "");

                Stopwatch stopWatch = new Stopwatch();
                stopWatch.Start();  

                Image<Gray, Byte> originPic = new Image<Gray, byte>(fName);
                Image<Bgr, Byte> originBgrPic = new Image<Bgr, byte>(fName);
                Bitmap bitmapGrayPic = originPic.ToBitmap();
                Bitmap bitmapPicChanged = new Bitmap(bitmapGrayPic.Width, bitmapGrayPic.Height);
                for (int i = 0; i < bitmapPicChanged.Height; i++)
                {
                    for (int j = 0; j < bitmapPicChanged.Width; j++)
                    {
                        bitmapPicChanged.SetPixel(j, i, Color.FromArgb(255, 255, 255));
                    }
                }
                //局部平均
                Emgu.CV.Features2D.SIFTDetector sift = new Emgu.CV.Features2D.SIFTDetector
                    (0//特征点数目
                     , 5//octave层数
                     , 0.05//约束阈值
                     , 10//边界阈值
                     , 1.6//sigma
                    );
                Emgu.CV.Features2D.ImageFeature<float>[] siftPoint = sift.DetectFeatures(originPic, null);
                Emgu.CV.Util.VectorOfKeyPoint vectorOfPoint = new Emgu.CV.Util.VectorOfKeyPoint();
                vectorOfPoint.Clear();
                MKeyPoint[] mKeyPoint = new MKeyPoint[siftPoint.GetLength(0)];
                for (int i = 0; i < siftPoint.GetLength(0); i++)
                {
                    mKeyPoint[i].Point = siftPoint[i].KeyPoint.Point;
                }
                vectorOfPoint.Push(mKeyPoint);
                for (int i = 0; i < siftPoint.GetLength(0); i++)
                {
                    bitmapPicChanged.SetPixel(Convert.ToInt32(siftPoint[i].KeyPoint.Point.X), Convert.ToInt32(siftPoint[i].KeyPoint.Point.Y), Color.FromArgb(0, 0, 0));
                }
                string siftname = fName;
                siftname = fName.Remove(fName.Length - 4);
                siftname = siftname + "_basicSIFT.bmp";
                bitmapPicChanged.Save(siftname);
                Bgr c = new Bgr(Color.FromArgb(255, 0, 0));
                Image<Bgr, Byte> siftPic = Emgu.CV.Features2D.Features2DToolbox.DrawKeypoints(originBgrPic, vectorOfPoint, c, Emgu.CV.Features2D.Features2DToolbox.KeypointDrawType.DEFAULT);
                siftname = fName;
                siftname = fName.Remove(fName.Length - 4);
                siftname = siftname + "_SIFT.bmp";
                siftPic.Save(siftname);
                siftname = fName;
                siftname = fName.Remove(fName.Length - 4);
                siftname = siftname + "_SIFT.txt";
                FileStream fs = new FileStream(siftname, FileMode.Create);
                StreamWriter sw = new StreamWriter(fs);
                for (int i = 0; i < siftPoint.GetLength(0); i++)
                {
                    sw.Write(Convert.ToInt32(siftPoint[i].KeyPoint.Point.X));
                    sw.Write(' ');
                    sw.Write(Convert.ToInt32(siftPoint[i].KeyPoint.Point.Y));
                    sw.WriteLine();
                }
                sw.Close();
                fs.Close();
                stopWatch.Stop();
                TimeSpan ts2 = stopWatch.Elapsed;
                lblTime.Text = ts2.TotalMilliseconds.ToString();
                pictureBox1.Image = siftPic.ToBitmap();
            }
        }
    }
}
