﻿namespace CVsift
{
    partial class RealizeSIFT
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.OnepicOpen = new System.Windows.Forms.Button();
            this.TwopicOpen = new System.Windows.Forms.Button();
            this.OnepicBox = new System.Windows.Forms.PictureBox();
            this.TwopicBox = new System.Windows.Forms.PictureBox();
            this.openFileDialog1 = new System.Windows.Forms.OpenFileDialog();
            this.Search = new System.Windows.Forms.Button();
            this.textBox = new System.Windows.Forms.TextBox();
            ((System.ComponentModel.ISupportInitialize)(this.OnepicBox)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.TwopicBox)).BeginInit();
            this.SuspendLayout();
            // 
            // OnepicOpen
            // 
            this.OnepicOpen.Location = new System.Drawing.Point(12, 12);
            this.OnepicOpen.Name = "OnepicOpen";
            this.OnepicOpen.Size = new System.Drawing.Size(75, 23);
            this.OnepicOpen.TabIndex = 0;
            this.OnepicOpen.Text = "图片一";
            this.OnepicOpen.UseVisualStyleBackColor = true;
            this.OnepicOpen.Click += new System.EventHandler(this.OnepicOpen_Click);
            // 
            // TwopicOpen
            // 
            this.TwopicOpen.Location = new System.Drawing.Point(170, 12);
            this.TwopicOpen.Name = "TwopicOpen";
            this.TwopicOpen.Size = new System.Drawing.Size(75, 23);
            this.TwopicOpen.TabIndex = 1;
            this.TwopicOpen.Text = "图片二";
            this.TwopicOpen.UseVisualStyleBackColor = true;
            this.TwopicOpen.Click += new System.EventHandler(this.TwopicOpen_Click);
            // 
            // OnepicBox
            // 
            this.OnepicBox.Location = new System.Drawing.Point(12, 41);
            this.OnepicBox.Name = "OnepicBox";
            this.OnepicBox.Size = new System.Drawing.Size(282, 263);
            this.OnepicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.OnepicBox.TabIndex = 2;
            this.OnepicBox.TabStop = false;
            // 
            // TwopicBox
            // 
            this.TwopicBox.Location = new System.Drawing.Point(300, 41);
            this.TwopicBox.Name = "TwopicBox";
            this.TwopicBox.Size = new System.Drawing.Size(292, 263);
            this.TwopicBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
            this.TwopicBox.TabIndex = 3;
            this.TwopicBox.TabStop = false;
            // 
            // openFileDialog1
            // 
            this.openFileDialog1.FileName = "openFileDialog1";
            this.openFileDialog1.Filter = "图像文件 (*.BMP;*.JPG;*.GIF)|*.BMP;*.JPG;*.GIF|所有文件 (*.*)|*.*";
            // 
            // Search
            // 
            this.Search.Enabled = false;
            this.Search.Location = new System.Drawing.Point(325, 12);
            this.Search.Name = "Search";
            this.Search.Size = new System.Drawing.Size(79, 23);
            this.Search.TabIndex = 5;
            this.Search.Text = "寻找相似...";
            this.Search.UseVisualStyleBackColor = true;
            this.Search.Click += new System.EventHandler(this.Search_Click);
            // 
            // textBox
            // 
            this.textBox.Location = new System.Drawing.Point(480, 14);
            this.textBox.Name = "textBox";
            this.textBox.ReadOnly = true;
            this.textBox.Size = new System.Drawing.Size(112, 21);
            this.textBox.TabIndex = 6;
            // 
            // RealizeSIFT
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(773, 434);
            this.Controls.Add(this.textBox);
            this.Controls.Add(this.Search);
            this.Controls.Add(this.TwopicBox);
            this.Controls.Add(this.OnepicBox);
            this.Controls.Add(this.TwopicOpen);
            this.Controls.Add(this.OnepicOpen);
            this.Name = "RealizeSIFT";
            this.Text = "sift算法实现";
            ((System.ComponentModel.ISupportInitialize)(this.OnepicBox)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.TwopicBox)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Button OnepicOpen;
        private System.Windows.Forms.Button TwopicOpen;
        private System.Windows.Forms.PictureBox OnepicBox;
        private System.Windows.Forms.PictureBox TwopicBox;
        private System.Windows.Forms.OpenFileDialog openFileDialog1;
        private System.Windows.Forms.Button Search;
        private System.Windows.Forms.TextBox textBox;
    }
}

