﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using Emgu.CV;
using Emgu.CV.CvEnum;
using Emgu.CV.Structure;
using Emgu.Util;

namespace CVsift
{
    public partial class Form2 : Form
    {
        IntPtr img1;
        IntPtr img2;

        public Form2()
        {
            InitializeComponent();
        }

        //载入图像img1
        private void button1_Click(object sender, EventArgs e)
        {
            string strFileName = string.Empty;
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                img1 = CvInvoke.cvLoadImage(ofd.FileName, Emgu.CV.CvEnum.LOAD_IMAGE_TYPE.CV_LOAD_IMAGE_GRAYSCALE);
                Image<Gray, Byte> img_1 = new Image<Gray, Byte>(CvInvoke.cvGetSize(img1));
                CvInvoke.cvCopy(img1, img_1, IntPtr.Zero);
                imageBox1.Image = img_1;


            }
        }

        //载入图像img2
        private void button2_Click(object sender, EventArgs e)
        {
            string strFileName = string.Empty;
            OpenFileDialog ofd = new OpenFileDialog();
            if (ofd.ShowDialog() == DialogResult.OK)
            {
                img2 = CvInvoke.cvLoadImage(ofd.FileName, Emgu.CV.CvEnum.LOAD_IMAGE_TYPE.CV_LOAD_IMAGE_GRAYSCALE);
                Image<Gray, Byte> img_2 = new Image<Gray, Byte>(CvInvoke.cvGetSize(img2));
                CvInvoke.cvCopy(img2, img_2, IntPtr.Zero);
                imageBox2.Image = img_2;
            }
        }

        //相减计算
        private void button3_Click(object sender, EventArgs e)
        {
            Image<Gray, Byte> img_1 = new Image<Gray, Byte>(CvInvoke.cvGetSize(img1));
            Image<Gray, Byte> img_2 = new Image<Gray, Byte>(CvInvoke.cvGetSize(img2));
            Image<Gray, Byte> result = new Image<Gray, Byte>(CvInvoke.cvGetSize(img2));
            CvInvoke.cvCopy(img1, img_1, IntPtr.Zero);
            CvInvoke.cvCopy(img2, img_2, IntPtr.Zero);

            result = img_1.AbsDiff(img_2);
            imageBox3.Image = result;

        } 


    }
}
