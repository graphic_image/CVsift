﻿namespace CVsift
{
    partial class Form4
    {
        /// <summary>
        /// 必需的设计器变量。
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// 清理所有正在使用的资源。
        /// </summary>
        /// <param name="disposing">如果应释放托管资源，为 true；否则为 false。</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows 窗体设计器生成的代码

        /// <summary>
        /// 设计器支持所需的方法 - 不要
        /// 使用代码编辑器修改此方法的内容。
        /// </summary>
        private void InitializeComponent()
        {
            this.menuStrip1 = new System.Windows.Forms.MenuStrip();
            this.文件ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.打开标准图像ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.退出ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.图像显示方式ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.操作ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.灰度ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.二值ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.求边缘ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.求轮廓ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.splitContainer1 = new System.Windows.Forms.SplitContainer();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.panel1 = new System.Windows.Forms.Panel();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.splitContainer2 = new System.Windows.Forms.SplitContainer();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.pictureBox2 = new System.Windows.Forms.PictureBox();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.textBox2 = new System.Windows.Forms.TextBox();
            this.textBox1 = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.checkBox_Contour = new System.Windows.Forms.CheckBox();
            this.Binary = new System.Windows.Forms.CheckBox();
            this.checkBox_canny = new System.Windows.Forms.CheckBox();
            this.Gray = new System.Windows.Forms.CheckBox();
            this.标准图像处理ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.图像匹配ToolStripMenuItem = new System.Windows.Forms.ToolStripMenuItem();
            this.menuStrip1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).BeginInit();
            this.splitContainer1.Panel1.SuspendLayout();
            this.splitContainer1.Panel2.SuspendLayout();
            this.splitContainer1.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).BeginInit();
            this.splitContainer2.Panel1.SuspendLayout();
            this.splitContainer2.Panel2.SuspendLayout();
            this.splitContainer2.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // menuStrip1
            // 
            this.menuStrip1.BackColor = System.Drawing.SystemColors.GradientActiveCaption;
            this.menuStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.文件ToolStripMenuItem,
            this.图像显示方式ToolStripMenuItem,
            this.操作ToolStripMenuItem,
            this.图像匹配ToolStripMenuItem});
            this.menuStrip1.Location = new System.Drawing.Point(0, 0);
            this.menuStrip1.Name = "menuStrip1";
            this.menuStrip1.Size = new System.Drawing.Size(804, 24);
            this.menuStrip1.TabIndex = 0;
            this.menuStrip1.Text = "menuStrip1";
            // 
            // 文件ToolStripMenuItem
            // 
            this.文件ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.打开标准图像ToolStripMenuItem,
            this.退出ToolStripMenuItem});
            this.文件ToolStripMenuItem.Name = "文件ToolStripMenuItem";
            this.文件ToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.文件ToolStripMenuItem.Text = "文件";
            // 
            // 打开标准图像ToolStripMenuItem
            // 
            this.打开标准图像ToolStripMenuItem.Name = "打开标准图像ToolStripMenuItem";
            this.打开标准图像ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.打开标准图像ToolStripMenuItem.Text = "打开标准图像";
            this.打开标准图像ToolStripMenuItem.Click += new System.EventHandler(this.打开标准图像ToolStripMenuItem_Click);
            // 
            // 退出ToolStripMenuItem
            // 
            this.退出ToolStripMenuItem.Name = "退出ToolStripMenuItem";
            this.退出ToolStripMenuItem.Size = new System.Drawing.Size(146, 22);
            this.退出ToolStripMenuItem.Text = "退出";
            this.退出ToolStripMenuItem.Click += new System.EventHandler(this.退出ToolStripMenuItem_Click);
            // 
            // 图像显示方式ToolStripMenuItem
            // 
            this.图像显示方式ToolStripMenuItem.Name = "图像显示方式ToolStripMenuItem";
            this.图像显示方式ToolStripMenuItem.Size = new System.Drawing.Size(91, 20);
            this.图像显示方式ToolStripMenuItem.Text = "图像显示方式";
            // 
            // 操作ToolStripMenuItem
            // 
            this.操作ToolStripMenuItem.DropDownItems.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.灰度ToolStripMenuItem,
            this.二值ToolStripMenuItem,
            this.求边缘ToolStripMenuItem,
            this.求轮廓ToolStripMenuItem,
            this.标准图像处理ToolStripMenuItem});
            this.操作ToolStripMenuItem.Name = "操作ToolStripMenuItem";
            this.操作ToolStripMenuItem.Size = new System.Drawing.Size(43, 20);
            this.操作ToolStripMenuItem.Text = "操作";
            // 
            // 灰度ToolStripMenuItem
            // 
            this.灰度ToolStripMenuItem.Name = "灰度ToolStripMenuItem";
            this.灰度ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.灰度ToolStripMenuItem.Text = "转换成灰度图";
            this.灰度ToolStripMenuItem.Click += new System.EventHandler(this.转换成灰度图ToolStripMenuItem_Click);
            // 
            // 二值ToolStripMenuItem
            // 
            this.二值ToolStripMenuItem.Name = "二值ToolStripMenuItem";
            this.二值ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.二值ToolStripMenuItem.Text = "二值化";
            this.二值ToolStripMenuItem.Click += new System.EventHandler(this.二值化ToolStripMenuItem_Click);
            // 
            // 求边缘ToolStripMenuItem
            // 
            this.求边缘ToolStripMenuItem.Name = "求边缘ToolStripMenuItem";
            this.求边缘ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.求边缘ToolStripMenuItem.Text = "求边缘";
            this.求边缘ToolStripMenuItem.Click += new System.EventHandler(this.求边缘ToolStripMenuItem_Click);
            // 
            // 求轮廓ToolStripMenuItem
            // 
            this.求轮廓ToolStripMenuItem.Name = "求轮廓ToolStripMenuItem";
            this.求轮廓ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.求轮廓ToolStripMenuItem.Text = "求轮廓";
            this.求轮廓ToolStripMenuItem.Click += new System.EventHandler(this.求轮廓ToolStripMenuItem_Click);
            // 
            // splitContainer1
            // 
            this.splitContainer1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer1.Location = new System.Drawing.Point(0, 24);
            this.splitContainer1.Name = "splitContainer1";
            // 
            // splitContainer1.Panel1
            // 
            this.splitContainer1.Panel1.AutoScroll = true;
            this.splitContainer1.Panel1.Controls.Add(this.groupBox1);
            // 
            // splitContainer1.Panel2
            // 
            this.splitContainer1.Panel2.Controls.Add(this.splitContainer2);
            this.splitContainer1.Size = new System.Drawing.Size(804, 422);
            this.splitContainer1.SplitterDistance = 480;
            this.splitContainer1.TabIndex = 1;
            this.splitContainer1.TabStop = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox1.Controls.Add(this.panel1);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(475, 415);
            this.groupBox1.TabIndex = 1;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "实时采集图像";
            // 
            // panel1
            // 
            this.panel1.AutoScroll = true;
            this.panel1.Controls.Add(this.pictureBox1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 17);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(469, 395);
            this.panel1.TabIndex = 0;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Location = new System.Drawing.Point(0, 2);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(469, 395);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox1.TabIndex = 0;
            this.pictureBox1.TabStop = false;
            // 
            // splitContainer2
            // 
            this.splitContainer2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.splitContainer2.Location = new System.Drawing.Point(0, 0);
            this.splitContainer2.Name = "splitContainer2";
            this.splitContainer2.Orientation = System.Windows.Forms.Orientation.Horizontal;
            // 
            // splitContainer2.Panel1
            // 
            this.splitContainer2.Panel1.Controls.Add(this.groupBox2);
            // 
            // splitContainer2.Panel2
            // 
            this.splitContainer2.Panel2.Controls.Add(this.groupBox3);
            this.splitContainer2.Size = new System.Drawing.Size(320, 422);
            this.splitContainer2.SplitterDistance = 252;
            this.splitContainer2.TabIndex = 0;
            this.splitContainer2.TabStop = false;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.SystemColors.Control;
            this.groupBox2.Controls.Add(this.panel2);
            this.groupBox2.Location = new System.Drawing.Point(3, 3);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(314, 244);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "标准图像";
            // 
            // panel2
            // 
            this.panel2.AutoScroll = true;
            this.panel2.Controls.Add(this.pictureBox2);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 17);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(308, 224);
            this.panel2.TabIndex = 0;
            // 
            // pictureBox2
            // 
            this.pictureBox2.Location = new System.Drawing.Point(0, 0);
            this.pictureBox2.Name = "pictureBox2";
            this.pictureBox2.Size = new System.Drawing.Size(308, 224);
            this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.AutoSize;
            this.pictureBox2.TabIndex = 0;
            this.pictureBox2.TabStop = false;
            // 
            // groupBox3
            // 
            this.groupBox3.Controls.Add(this.textBox2);
            this.groupBox3.Controls.Add(this.textBox1);
            this.groupBox3.Controls.Add(this.label2);
            this.groupBox3.Controls.Add(this.label1);
            this.groupBox3.Controls.Add(this.checkBox_Contour);
            this.groupBox3.Controls.Add(this.Binary);
            this.groupBox3.Controls.Add(this.checkBox_canny);
            this.groupBox3.Controls.Add(this.Gray);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(311, 158);
            this.groupBox3.TabIndex = 0;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "操作及结果";
            // 
            // textBox2
            // 
            this.textBox2.Location = new System.Drawing.Point(146, 130);
            this.textBox2.Name = "textBox2";
            this.textBox2.Size = new System.Drawing.Size(124, 21);
            this.textBox2.TabIndex = 2;
            // 
            // textBox1
            // 
            this.textBox1.Location = new System.Drawing.Point(146, 98);
            this.textBox1.Name = "textBox1";
            this.textBox1.Size = new System.Drawing.Size(124, 21);
            this.textBox1.TabIndex = 2;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(13, 133);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 12);
            this.label2.TabIndex = 1;
            this.label2.Text = "两幅图像的匹配度：";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(13, 101);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 12);
            this.label1.TabIndex = 1;
            this.label1.Text = "单幅图处理时间：";
            // 
            // checkBox_Contour
            // 
            this.checkBox_Contour.AutoSize = true;
            this.checkBox_Contour.Location = new System.Drawing.Point(146, 67);
            this.checkBox_Contour.Name = "checkBox_Contour";
            this.checkBox_Contour.Size = new System.Drawing.Size(66, 16);
            this.checkBox_Contour.TabIndex = 0;
            this.checkBox_Contour.Text = "Contour";
            this.checkBox_Contour.UseVisualStyleBackColor = true;
            this.checkBox_Contour.CheckedChanged += new System.EventHandler(this.checkBox_Contour_CheckedChanged);
            // 
            // Binary
            // 
            this.Binary.AutoSize = true;
            this.Binary.Location = new System.Drawing.Point(146, 31);
            this.Binary.Name = "Binary";
            this.Binary.Size = new System.Drawing.Size(60, 16);
            this.Binary.TabIndex = 0;
            this.Binary.Text = "Binary";
            this.Binary.UseVisualStyleBackColor = true;
            this.Binary.CheckedChanged += new System.EventHandler(this.Binary_CheckedChanged);
            // 
            // checkBox_canny
            // 
            this.checkBox_canny.AutoSize = true;
            this.checkBox_canny.Location = new System.Drawing.Point(23, 67);
            this.checkBox_canny.Name = "checkBox_canny";
            this.checkBox_canny.Size = new System.Drawing.Size(54, 16);
            this.checkBox_canny.TabIndex = 0;
            this.checkBox_canny.Text = "Canny";
            this.checkBox_canny.UseVisualStyleBackColor = true;
            this.checkBox_canny.CheckedChanged += new System.EventHandler(this.checkBox_canny_CheckedChanged);
            // 
            // Gray
            // 
            this.Gray.AutoSize = true;
            this.Gray.Location = new System.Drawing.Point(23, 31);
            this.Gray.Name = "Gray";
            this.Gray.Size = new System.Drawing.Size(48, 16);
            this.Gray.TabIndex = 0;
            this.Gray.Text = "Gray";
            this.Gray.UseVisualStyleBackColor = true;
            this.Gray.CheckedChanged += new System.EventHandler(this.Gray_CheckedChanged);
            // 
            // 标准图像处理ToolStripMenuItem
            // 
            this.标准图像处理ToolStripMenuItem.Name = "标准图像处理ToolStripMenuItem";
            this.标准图像处理ToolStripMenuItem.Size = new System.Drawing.Size(152, 22);
            this.标准图像处理ToolStripMenuItem.Text = "标准图像处理";
            this.标准图像处理ToolStripMenuItem.Click += new System.EventHandler(this.标准图像处理ToolStripMenuItem_Click);
            // 
            // 图像匹配ToolStripMenuItem
            // 
            this.图像匹配ToolStripMenuItem.Name = "图像匹配ToolStripMenuItem";
            this.图像匹配ToolStripMenuItem.Size = new System.Drawing.Size(67, 20);
            this.图像匹配ToolStripMenuItem.Text = "图像匹配";
            this.图像匹配ToolStripMenuItem.Click += new System.EventHandler(this.图像匹配ToolStripMenuItem_Click);
            // 
            // Form1
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 12F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.Control;
            this.ClientSize = new System.Drawing.Size(804, 446);
            this.Controls.Add(this.splitContainer1);
            this.Controls.Add(this.menuStrip1);
            this.MainMenuStrip = this.menuStrip1;
            this.Name = "Form1";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Form1";
            this.menuStrip1.ResumeLayout(false);
            this.menuStrip1.PerformLayout();
            this.splitContainer1.Panel1.ResumeLayout(false);
            this.splitContainer1.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer1)).EndInit();
            this.splitContainer1.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.splitContainer2.Panel1.ResumeLayout(false);
            this.splitContainer2.Panel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.splitContainer2)).EndInit();
            this.splitContainer2.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.MenuStrip menuStrip1;
        private System.Windows.Forms.ToolStripMenuItem 文件ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 打开标准图像ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 退出ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 图像显示方式ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 操作ToolStripMenuItem;
        private System.Windows.Forms.SplitContainer splitContainer1;
        private System.Windows.Forms.SplitContainer splitContainer2;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.ToolStripMenuItem 二值ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 灰度ToolStripMenuItem;
        private System.Windows.Forms.CheckBox Binary;
        private System.Windows.Forms.CheckBox Gray;
        private System.Windows.Forms.TextBox textBox2;
        private System.Windows.Forms.TextBox textBox1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox checkBox_Contour;
        private System.Windows.Forms.CheckBox checkBox_canny;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.ToolStripMenuItem 求边缘ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 求轮廓ToolStripMenuItem;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolStripMenuItem 标准图像处理ToolStripMenuItem;
        private System.Windows.Forms.ToolStripMenuItem 图像匹配ToolStripMenuItem;
    }
}

